output "distribution_id" {
  value = module.extended.distribution_id
}

output "distribution_arn" {
  value = module.extended.distribution_arn
}

output "distribution_caller_reference" {
  value = module.extended.distribution_caller_reference
}

output "distribution_status" {
  value = module.extended.distribution_status
}

output "distribution_trusted_signers" {
  value = module.extended.distribution_trusted_signers
}

output "distribution_domain_name" {
  value = module.extended.distribution_domain_name
}

output "distribution_last_modified_time" {
  value = module.extended.distribution_last_modified_time
}

output "distribution_in_progress_validation_batches" {
  value = module.extended.distribution_in_progress_validation_batches
}

output "distribution_etag" {
  value = module.extended.distribution_etag
}

output "distribution_hosted_zone_id" {
  value = module.extended.distribution_hosted_zone_id
}

output "origin_access_identity_id" {
  value = module.extended.origin_access_identity_id
}

output "origin_access_identity_iam_arn" {
  value = module.extended.origin_access_identity_iam_arn
}

output "monitoring_subscription_id" {
  value = module.extended.monitoring_subscription_id
}
