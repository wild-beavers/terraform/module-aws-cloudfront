## 2.2.1

- maintenance: migrate module to Terraform registry
- chore: bump pre-commit hooks

## 2.2.0

- feat: adds trusted key groups with trusted public key, controlled by `var.trusted_key_groups`
- feat: adds `apply_local_trusted_key_groups` option to ordered_cache_behaviors and default_cache_behavior, to control whether `var.trusted_key_groups` should be applied

## 2.1.2

- fix: validation on `var.origin_access_control_description`
- chore: bump pre-commit hook

## 2.1.1

- fix: running condition that prevent Origin Access Identity usage

## 2.1.0

- feat: adds Origin Access Control. This introduces “var.origin_access_control_enabled”, “var.origin_access_control_name”, “var.origin_access_control_description” and “var.origin_access_control_signing_behavior”

## 2.0.0

- refactor: (BREAKING) “var.default_cache_behavior.lambda_function_associations” and “var.default_cache_behavior.function_associations” are now a map of object. The attribute `event_type` is now replace by the map key. The same behaviour applies to “var.ordered_cache_behaviors”
- fix: validation on lambda functions regex

## 1.0.2

- fix: wrong validation with null values on `var.custom_error_response`

## 1.0.1

- fix: wrong validation for `var.custom_error_response`
- chore: remove unused `try` for all outputs
- chore: bump pre-commit hooks
- test: adds .gitlab.yml file
- test: rename test files from `main.tf` to `deploy.tf`
- test: adds disable example

## 1.0.0

- refactor: (BREAKING) adds variable validation and do various changes on
- tests: adds a default simple test
- chore: replaces Apache 2.0 licence to MIT
- chore: bumps pre-commit hooks

## 0.0.0

- tech: init
