# AWS CloudFront Terraform module

Terraform module which creates AWS CloudFront resources with all (or almost all) features provided by Terraform AWS provider.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13.1 |
| aws | >= 4.29 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 4.29 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_cloudfront_distribution.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_distribution) | resource |
| [aws_cloudfront_key_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_key_group) | resource |
| [aws_cloudfront_monitoring_subscription.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_monitoring_subscription) | resource |
| [aws_cloudfront_origin_access_control.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_origin_access_control) | resource |
| [aws_cloudfront_origin_access_identity.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_origin_access_identity) | resource |
| [aws_cloudfront_public_key.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_public_key) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| aliases | Extra CNAMEs (alternate domain names), if any, for this distribution. | `list(string)` | `[]` | no |
| comment | Any comments you want to include about the distribution. | `string` | `null` | no |
| custom\_error\_response | One or more custom error response elements | <pre>list(object({<br/>    error_code = number<br/><br/>    response_code         = optional(number)<br/>    response_page_path    = optional(string)<br/>    error_caching_min_ttl = optional(number)<br/>  }))</pre> | `null` | no |
| default\_cache\_behavior | The default cache behavior for this distribution | <pre>object({<br/>    target_origin_id               = string<br/>    viewer_protocol_policy         = string<br/>    allowed_methods                = list(string)<br/>    cached_methods                 = list(string)<br/>    compress                       = optional(bool, false)<br/>    field_level_encryption_id      = optional(string)<br/>    smooth_streaming               = optional(bool, false)<br/>    trusted_signers                = optional(list(string), [])<br/>    trusted_key_groups             = optional(list(string), [])<br/>    apply_local_trusted_key_groups = optional(bool, true)<br/>    cache_policy_id                = optional(string)<br/>    origin_request_policy_id       = optional(string)<br/>    origin_access_control_id       = optional(string, "self")<br/>    response_headers_policy_id     = optional(string)<br/>    realtime_log_config_arn        = optional(string)<br/>    min_ttl                        = optional(number)<br/>    default_ttl                    = optional(number)<br/>    max_ttl                        = optional(number)<br/>    forwarded_values = optional(object({<br/>      query_string            = string<br/>      query_string_cache_keys = optional(list(string), [])<br/>      headers                 = optional(list(string), [])<br/>      cookies = optional(object({<br/>        forward           = string<br/>        whitelisted_names = optional(list(string), [])<br/>      }), null)<br/>    }), null)<br/>    lambda_function_associations = optional(map(object({<br/>      lambda_arn   = string<br/>      include_body = optional(bool, false)<br/>    })), {})<br/>    function_associations = optional(map(object({<br/>      function_arn = string<br/>    })), {})<br/>  })</pre> | `null` | no |
| default\_root\_object | The object that you want CloudFront to return (for example, index.html) when an end user requests the root URL. | `string` | `null` | no |
| enabled | Whether the distribution is enabled to accept end user requests for content. | `bool` | `true` | no |
| geo\_restriction | The restriction configuration for this distribution (geo\_restrictions) | <pre>object({<br/>    restriction_type = string<br/>    locations        = optional(list(string), [])<br/>  })</pre> | <pre>{<br/>  "restriction_type": "none"<br/>}</pre> | no |
| http\_version | The maximum HTTP version to support on the distribution. Allowed values are http1.1, http2, http2and3 and http3. The default is http3. | `string` | `"http3"` | no |
| ipv6\_enabled | Whether to enable IPv6 or not for the distribution. | `bool` | `false` | no |
| logging\_config | The logging configuration that controls how logs are written to your distribution (maximum one). | <pre>object({<br/>    bucket          = string<br/>    prefix          = optional(string)<br/>    include_cookies = optional(bool, false)<br/>  })</pre> | `null` | no |
| monitoring\_subscription\_enabled | Whether to create the monitoring subscription or not. | `bool` | `false` | no |
| ordered\_cache\_behaviors | An ordered list of cache behaviors resource for this distribution. List from top to bottom in order of precedence. The topmost cache behavior will have precedence 0. | <pre>list(<br/>    object({<br/>      path_pattern                   = string<br/>      target_origin_id               = string<br/>      viewer_protocol_policy         = string<br/>      allowed_methods                = list(string)<br/>      cached_methods                 = list(string)<br/>      compress                       = optional(bool, false)<br/>      field_level_encryption_id      = optional(string)<br/>      smooth_streaming               = optional(bool, false)<br/>      trusted_signers                = optional(list(string), [])<br/>      trusted_key_groups             = optional(list(string), [])<br/>      apply_local_trusted_key_groups = optional(bool, true)<br/>      cache_policy_id                = optional(string)<br/>      origin_request_policy_id       = optional(string)<br/>      origin_access_control_id       = optional(string, "self")<br/>      response_headers_policy_id     = optional(string)<br/>      realtime_log_config_arn        = optional(string)<br/>      min_ttl                        = optional(number)<br/>      default_ttl                    = optional(number)<br/>      max_ttl                        = optional(number)<br/>      forwarded_values = optional(object({<br/>        query_string            = string<br/>        query_string_cache_keys = optional(list(string), [])<br/>        headers                 = optional(list(string), [])<br/>        cookies = optional(object({<br/>          forward           = string<br/>          whitelisted_names = optional(list(string), [])<br/>        }), null)<br/>      }), null)<br/>      lambda_function_associations = optional(map(object({<br/>        lambda_arn   = string<br/>        include_body = optional(bool, false)<br/>      })), {})<br/>      function_associations = optional(map(object({<br/>        function_arn = string<br/>      })), {})<br/>    })<br/>  )</pre> | `[]` | no |
| origin | One or more origins for this distribution (multiples allowed). | <pre>list(object({<br/>    domain_name              = string<br/>    origin_id                = string<br/>    origin_path              = optional(string, "")<br/>    connection_attempts      = optional(number, 3)<br/>    connection_timeout       = optional(number, 10)<br/>    origin_access_control_id = optional(string)<br/>    s3_origin_config = optional(object({<br/>      origin_access_identity = optional(string, null)<br/>    }), null)<br/>    custom_origin_config = optional(object({<br/>      http_port                = optional(number, 80)<br/>      https_port               = optional(number, 443)<br/>      origin_protocol_policy   = optional(string, "https-only")<br/>      origin_ssl_protocols     = optional(list(string), ["TLSv1.2"])<br/>      origin_keepalive_timeout = optional(number, 5)<br/>      origin_read_timeout      = optional(number, 30)<br/>    }), null)<br/>    custom_header = optional(list(object({<br/>      name  = optional(string, "")<br/>      value = optional(string, "")<br/>    })), [])<br/>    origin_shield = optional(object({<br/>      enabled              = optional(bool, true)<br/>      origin_shield_region = optional(string, "us-east-1")<br/>    }), null)<br/>  }))</pre> | `[]` | no |
| origin\_access\_control\_description | The description of the Origin Access Control. | `string` | `null` | no |
| origin\_access\_control\_enabled | Whether to create the Origin Access Control or not | `bool` | `false` | no |
| origin\_access\_control\_name | A name that identifies the Origin Access Control. | `string` | `null` | no |
| origin\_access\_control\_signing\_behavior | Specifies which requests CloudFront signs. | `string` | `"always"` | no |
| origin\_access\_identity\_comment | CloudFront origin access identities comment. | `string` | `null` | no |
| origin\_access\_identity\_enabled | Whether to create the CloudFront Origin Access Identity or not | `bool` | `false` | no |
| origin\_groups | One or more origin\_group for this distribution (multiples allowed). | <pre>list(object({<br/>    origin_id                  = string<br/>    failover_status_codes      = list(number)<br/>    primary_member_origin_id   = string<br/>    secondary_member_origin_id = string<br/>  }))</pre> | `[]` | no |
| price\_class | The price class for this distribution. One of PriceClass\_All, PriceClass\_200, PriceClass\_100 | `string` | `null` | no |
| realtime\_metrics\_subscription\_status | A flag that indicates whether additional CloudWatch metrics are enabled for a given CloudFront distribution. Valid values are `Enabled` and `Disabled`. | `string` | `"Enabled"` | no |
| retain\_on\_delete | Whether tp disables the distribution instead of deleting it when destroying the resource through Terraform or not. If this is set, the distribution needs to be deleted manually afterwards. | `bool` | `false` | no |
| tags | A map of tags to assign to the resource. | `map(string)` | `{}` | no |
| trusted\_key\_groups | One or more key groups to associate with the cache behaviors.<br/>`apply_local_trusted_key_groups` control whether to attach these `var.trusted_key_groups` to the cache behaviors.<br/>Keys are free values.<br/><br/>    * name    (required, string):      A name to identify the key group.<br/>    * items   (required, map(string)): A list of public key material<br/>      * encoded\_key (required, string):     The encoded public key that you want to add to CloudFront to use with features like field-level encryption. Must end with newline.<br/>      * comment     (optional, string, ""): An optional comment about the public key.<br/>      * name        (optional, string):     The name for the public key. By default generated by Terraform.<br/>    * comment (optional, string, ""):  A comment to describe the key group. | <pre>map(object({<br/>    name = string<br/>    items = map(object({<br/>      encoded_key = string<br/>      name        = optional(string)<br/>      comment     = optional(string, "")<br/>    }))<br/>    comment = optional(string, "")<br/>  }))</pre> | `{}` | no |
| viewer\_certificate | The SSL configuration for this distribution | <pre>object({<br/>    acm_certificate_arn            = optional(string)<br/>    cloudfront_default_certificate = optional(bool, false)<br/>    iam_certificate_id             = optional(string)<br/><br/>    minimum_protocol_version = optional(string)<br/>    ssl_support_method       = optional(string)<br/>  })</pre> | <pre>{<br/>  "cloudfront_default_certificate": true<br/>}</pre> | no |
| wait\_for\_deployment | Whether to wait for the distribution status to change from InProgress to Deployed or not | `bool` | `true` | no |
| web\_acl\_id | If you're using AWS WAF to filter CloudFront requests, the Id of the AWS WAF web ACL that is associated with the distribution. The WAF Web ACL must exist in the WAF Global (CloudFront) region and the credentials configuring this argument must have waf:GetWebACL permissions assigned. If using WAFv2, provide the ARN of the web ACL. | `string` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| distribution\_arn | n/a |
| distribution\_caller\_reference | n/a |
| distribution\_domain\_name | n/a |
| distribution\_etag | n/a |
| distribution\_hosted\_zone\_id | n/a |
| distribution\_id | n/a |
| distribution\_in\_progress\_validation\_batches | n/a |
| distribution\_last\_modified\_time | n/a |
| distribution\_status | n/a |
| distribution\_trusted\_signers | n/a |
| monitoring\_subscription\_id | n/a |
| origin\_access\_control\_id | n/a |
| origin\_access\_identity\_iam\_arn | n/a |
| origin\_access\_identity\_id | n/a |
| trusted\_key\_groups | n/a |
<!-- END_TF_DOCS -->

## Versioning
This repository follows [Semantic Versioning 2.0.0](https://semver.org/)

## Git Hooks
This repository uses [pre-commit](https://pre-commit.com/) hooks.
