output "distribution_id" {
  value = aws_cloudfront_distribution.this["0"].id
}

output "distribution_arn" {
  value = aws_cloudfront_distribution.this["0"].arn
}

output "distribution_caller_reference" {
  value = aws_cloudfront_distribution.this["0"].caller_reference
}

output "distribution_status" {
  value = aws_cloudfront_distribution.this["0"].status
}

output "distribution_trusted_signers" {
  value = aws_cloudfront_distribution.this["0"].trusted_signers
}

output "distribution_domain_name" {
  value = aws_cloudfront_distribution.this["0"].domain_name
}

output "distribution_last_modified_time" {
  value = aws_cloudfront_distribution.this["0"].last_modified_time
}

output "distribution_in_progress_validation_batches" {
  value = aws_cloudfront_distribution.this["0"].in_progress_validation_batches
}

output "distribution_etag" {
  value = aws_cloudfront_distribution.this["0"].etag
}

output "distribution_hosted_zone_id" {
  value = aws_cloudfront_distribution.this["0"].hosted_zone_id
}

output "origin_access_identity_id" {
  value = var.origin_access_identity_enabled == true ? aws_cloudfront_origin_access_identity.this["0"].id : null
}

output "origin_access_identity_iam_arn" {
  value = var.origin_access_identity_enabled == true ? aws_cloudfront_origin_access_identity.this["0"].iam_arn : null
}

output "monitoring_subscription_id" {
  value = var.monitoring_subscription_enabled == true ? aws_cloudfront_monitoring_subscription.this[0].id : null
}

output "origin_access_control_id" {
  value = var.origin_access_control_enabled ? aws_cloudfront_origin_access_control.this["0"].id : null
}
